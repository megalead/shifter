var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render(
    'index',
    {
      title: 'Дополнительные сервисы',
      services: [
        {title: 'Превращатор', link: 'shifter'}
      ]
    }
  );
});

module.exports = router;
