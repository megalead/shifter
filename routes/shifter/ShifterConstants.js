'use strict';

var readFile = require('./functions/readFile');

const ADD_INPUTS = readFile(`${__dirname}/includes/addInputs.php`);
const HEAD_INCLUDE = readFile(`${__dirname}/includes/headInclude.php`);

module.exports = {
  TPL_DIR: 'www',  // Обозначение каталога с шаблонами
  P_DIR: 'p',      // Обозначение каталога с партнерскими копиями

  // Рутовский каталог с доменами
  DOMAINS_DIR: process.env.DOMAINS_DIR || '/home/sftp/www',

  // Дополнительные поля подклчаемые к формам
  ADD_INPUTS: ADD_INPUTS,

  // Подклчаем PHP ГОЛОВУ
  HEAD_INCLUDE: HEAD_INCLUDE,

  // Файл обработчика формы
  POST_CONFIRM_FILE: `${__dirname}/includes/post_confirm_success.php`

}
