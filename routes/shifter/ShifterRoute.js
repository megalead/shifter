var express = require('express');
var router = express.Router();
var multer  =   require('multer');
var path = require('path');

var getFoldersMap = require('./functions/getFoldersMap');
var ShifterFolder = require('./classes/ShifterFolder');
var calcFolderType = require('./functions/calcFolderType');
var getFolderFamily = require('./functions/getFolderFamily');

// ########################### SHifter ROOT  ###################################

/* GET home page. */
router.get('/', function(req, res, next) {
  var htmlPath = path.resolve('public/shifter.html');
  return res.sendFile(htmlPath);
});

router.get('/getTree', function(req, res, next) {

  // Загружем карту каталогов
  getFoldersMap((foldersMap) => {
    res.setHeader('Content-Type', 'application/json');
    res.send(JSON.stringify({ foldersMap: foldersMap }, null, 4));
  });

});

// ########################### saveFolderOptions ###############################

router.post('/saveFolderOptions', function(req, res, next) {
  try {
    data = [];
    for(var element in req.body) {
      data.push({[element] : req.body[element]});
    }

    // Создаем объект управляющий каталогами
    var folder = new ShifterFolder(req.body);

    // Передаем новые параметры
    var result = folder.setOptions(req.body.options);

    if(result) {
      res.setHeader('Content-Type', 'application/json');
      res.send(JSON.stringify({
        message: `Данные успешно записаны`,
        data: folder._folderOptions,
      }, null, 4));
    } else { throw new Error('Произошла ошибка во время записи опций'); }
  } catch (e) {
    res.status(422);
    res.setHeader('Content-Type', 'application/json');
    res.send(JSON.stringify({message: `${e.message}`,}, null, 4));
  }
});

// ########################### renameFolder  ###################################

router.get('/renameFolder', function(req, res, next) {
  var data = {path: req.param('path'), name: req.param('name'),}

  try {
    var folder = new ShifterFolder(data);
    var result = folder.renameFolder(data.name);

    res.setHeader('Content-Type', 'application/json');
    res.send(JSON.stringify({
      message: 'Каталог успешно переименован',
      path: result,
    }, null, 4));

  } catch (e) {
    res.status(422);
    res.setHeader('Content-Type', 'application/json');
    res.send(JSON.stringify({message: `${e.message}`,}, null, 4));
  }

});

// ########################### fillTemplate  ###################################


router.get('/fillTemplate', function(req, res, nex) {
  var data = {path: req.param('path'), name: req.param('name'),}
  var Folder = new ShifterFolder(data);
  var result = Folder.fillTemplate();

  res.setHeader('Content-Type', 'application/json');
  res.send(JSON.stringify(result, null, 4));
});

// ################# createNewPartnerFolder  ###################################

router.get('/createNewPartnerFolder', function(req, res, next) {
  var data = {path: req.param('path'), newname: req.param('newname'),}
  var Folder = new ShifterFolder(data);

  try {
    // Создаем новый каталог
    result = Folder.createChildFolder(data.newname);
    if(!result) throw new Error('Каталог уже существует');

    // Загружем свежую карту каталогов
    getFoldersMap((foldersMap) => {
      res.setHeader('Content-Type', 'application/json');
      res.send(JSON.stringify({ foldersMap: foldersMap }, null, 4));
    });

  } catch (e) {
    // Загружем свежую карту каталогов
    getFoldersMap((foldersMap) => {
      res.status(422);
      res.setHeader('Content-Type', 'application/json');
      res.send(JSON.stringify({
        message: `${e.message}`,
        foldersMap: foldersMap,
      }, null, 4));
    });

  }

});

// ################# fillTemplateToChilds  #####################################

router.get('/fillTemplateToChilds', function(req, res, next) {
  try {
    var Folder = new ShifterFolder({path: req.param('path')});
    var result = Folder.fillTemplateToChilds();
    res.setHeader('Content-Type', 'application/json');
    res.send(JSON.stringify(result, null, 4));
  } catch (e) {
    console.error(e.stack);
    res.status(422);
    res.setHeader('Content-Type', 'application/json');
    res.send(JSON.stringify({message: `${e.message}`,}, null, 4));
  }
});

// ################# createNewDomainProject ####################################

router.get('/initNewDomainProject', function(req, res, next) {
  try {
    var Folder = new ShifterFolder({path: req.param('path')});
    var result = Folder.initNewDomainProject(req.param('newname'));

    if(!result) throw new Error('Каталог уже существует');

    // Загружем свежую карту каталогов
    getFoldersMap((foldersMap) => {
      res.setHeader('Content-Type', 'application/json');
      res.send(JSON.stringify({ foldersMap: foldersMap }, null, 4));
    });

  } catch (e) {
    console.error(`initNewDomainProject route error ====>>>> ${e.stack}`);
    // Загружем свежую карту каталогов
    getFoldersMap((foldersMap) => {
      res.status(422);
      res.setHeader('Content-Type', 'application/json');
      res.send(JSON.stringify({
        message: `${e.message}`,
        foldersMap: foldersMap,
      }, null, 4));
    });
  }
});

// ############################# uploadPostConfirmFile #########################
router.post('/uploadPostConfirmFile', function(req, res, next) {
  var path = '';

  var storage =   multer.diskStorage({
    destination: function (req, file, callback) {
      path = req.body.path;
      callback(null, path);
    },
    filename: function (req, file, callback) {callback(null, 'post_confirm_success.php');}
  });

  var upload = multer({ storage : storage}).single('file');

  upload(req,res,function(err) {
    if(err) {
      console.error(err);

      res.status(422);
      res.setHeader('Content-Type', 'application/json');
      return res.send(JSON.stringify({message: `Error uploading file.`,}));
    }
    res.setHeader('Content-Type', 'application/json');
    res.send(JSON.stringify({message: 'File is uploaded', }));
  });

});
// ============================ ТЕСТОВЫЕ МАРШРУТЫ ==============================

router.get('/testHtmlParser', function(req, res, next) {
  var Folder = new ShifterFolder({
    path: '/home/silent/public_html/tests/service/daily/powerbank/p/test1',
  });
  var result = Folder.prepareTemplate();
  res.setHeader('Content-Type', 'application/json');
  res.send(JSON.stringify(result, null, 4));
});

router.get('/testFolderCalc', function(req, res, next) {
  var result = calcFolderType(
    '/home/silent/public_html/tests/service/daily/powerbank/p'
  );
  res.setHeader('Content-Type', 'application/json');
  res.send(JSON.stringify({ result: result }, null, 4));
});

router.get('/testGetFamily', function(req, res, next) {
  var result = getFolderFamily(
    '/home/silent/public_html/tests/service/daily/powerbank/p/test1'
  );
  res.setHeader('Content-Type', 'application/json');
  res.send(JSON.stringify({ result: result }, null, 4));
});

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

module.exports = router;
