'use strict';

var fs = require('fs');
const folderOptionsFileName = 'shifter.json';

var writeFolderOptions = (folderPath, data) => {
  let filename = `${folderPath}/${folderOptionsFileName}`;

  try {
    fs.writeFileSync(filename, JSON.stringify(data));
    return true;
  } catch (e) {
    console.error(e.stack);
    throw new Error('Ошибка во время записи параметров каталога');
  }

}

module.exports = writeFolderOptions;
