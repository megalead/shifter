'use strict';

var fs = require('fs');
var fse = require('fs-extra');

let fillFolder = (dirFrom, dirTo) => {
  try {
    // Проверяем доступность каталогов
    fs.accessSync(dirFrom, fs.F_OK);
    fs.accessSync(dirTo, fs.F_OK);

    // Если оба марщрута являются каталогами
    if(fs.statSync(dirFrom).isDirectory() && fs.statSync(dirFrom).isDirectory()) {
      try {
        fse.copySync(dirFrom, dirTo);
      } catch (err) {
        console.error(err)
      }
      return {message: 'Шаблон был успешно залит'}
    } else { throw new Error('Маршрут не является каталогом'); }

  } catch (e) {
    console.log('===== Ошибка при слиянии каталогов =====>>>>', e.stack);
    throw new Error('Ошибка при слиянии каталогов');
  }
}

module.exports = fillFolder;
