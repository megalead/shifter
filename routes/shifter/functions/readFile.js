'use strict';

var fs = require('fs');

let readFile = (path) => {
  try {
    console.log('This is file', path);
    fs.accessSync(path, fs.F_OK);
    if(fs.statSync(path).isFile()) {
      return fs.readFileSync(path, 'utf-8');
    };
    throw new Error('Передан не файл');
  } catch (e) {
    console.error('Ошибка при записи файла', e.stack);
    throw new Error(e.message);
  }
}

module.exports = readFile;
