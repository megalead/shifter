'use strict';

var fs = require('fs');
var readDir = require('./readDir');

const DIR_EXCLUDES = ['.git'];

let getUncles = (parent, grandparent) => {
  var uncles = [];

  if(parent && grandparent) {

    console.log('Grandparent', grandparent);

    // Сканируем дедушку на каталоги
    readDir(grandparent.path, (grandFiles) => {
      for(let dir in grandFiles) {
        let dirName = grandFiles[dir];
        let dirPath = `${grandparent.path}/${dirName}`;
        let dirStat = fs.statSync(dirPath);

        if(dirStat.isDirectory() && dirName !== parent.name) {
          uncles.push({
            name: dirName,
            path: dirPath,
          });
        }
      }
    });

    return uncles;

  } else {
    throw new Error('Ошибка во время скинирования семьи. Не все параметры');
  }
}

let getChilds = (path) => {
  let childs = [];

  readDir(path, (files) => {
    for(let file in files) {
      let fileName = files[file];
      let filePath = `${path}/${fileName}`;

      try {
        let dirStats = fs.statSync(filePath);

        if(dirStats.isDirectory() && DIR_EXCLUDES.indexOf(fileName) === -1) {
          childs.push({
            name: fileName,
            path: filePath,
          });
        }
      } catch (e) {
        console.error(e.stack);
        throw new Error(e.message);
      }
    }
  });

  return childs;
}

let getFolderFamily = (path) => {
  let family = {};
  let splitPath = path.split('/');

  // Получаем родителя
  family.parent = {
    name: splitPath[splitPath.length-2],
    path: splitPath.slice(0, -1).join('/'),
  }

  // Получаем прародителя (дедулю)
  family.grandparent = {
    name: splitPath[splitPath.length-3],
    path: splitPath.slice(0, -2).join('/'),
  }

  // Получаем соседей родителя (дядь)
  family.uncles = getUncles(family.parent, family.grandparent);

  // Получить детей каталога
  family.childs = getChilds(path);

  return family;
}

module.exports = getFolderFamily;
