'use strict';

var fs = require('fs');

const folderOptionsFileName = 'shifter.json';

let getFolderOptions = (folderPath) => {
  let filename = `${folderPath}/${folderOptionsFileName}`;

  try {
    fs.accessSync(filename, fs.F_OK);
    return JSON.parse(fs.readFileSync(filename, 'utf8'));
  } catch (e) {
    return false;
  }
}

module.exports = getFolderOptions;
