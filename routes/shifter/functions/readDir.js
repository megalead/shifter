'use strict';

var fs = require('fs');

let readDir = (path, callback) => {
  let stats, files = [];

  try {
    fs.accessSync(path, fs.F_OK);
    stats = fs.statSync(path);

    if(stats.isDirectory()) {
      files = fs.readdirSync(path);
    }

    callback(files);

  } catch (e) {
    console.log(`#### Error access folder dir ####`, e.stack);
    throw new Error('Ошибка во время чтения каталога');
  }
}

module.exports = readDir;
