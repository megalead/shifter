'use strict';

let fs = require('fs');
let ShifterFolder = require('../classes/ShifterFolder');
let calcFolderType = require('./calcFolderType');
var getFolderOptions = require('./getFolderOptions');
let ShifterConstants = require('../ShifterConstants');

let rootFolder = ShifterConstants.DOMAINS_DIR;
let foldersMap = {rootFolder: rootFolder,};

let scanDir = (path, current) => {
  current.path = path;
  current.folders = [];

  let files = fs.readdirSync(path);

  // Если каталог не пустой
  if(files.length && current.name !== 'www') {

    // Перебираем файлы каталога
    for(let i = 0; i < files.length; i++) {

      // Формируем адрес текущ  его файла
      let filePath = `${path}/${files[i]}`;

      // Получаем сведеня о файле
      let stats = fs.statSync(filePath);

      // Если полученный файл является директорией
      if(stats.isDirectory() && files[i] !== `.git`) {

        // Ищем файл с опциями каталога
        let folderOptions = getFolderOptions(filePath);

        let folder = {
          name: files[i],
          path: filePath,
          folders: [],
        };
        current.folders.push(folder);

        // Присваиваем вычисляемые значения каталога
        folder.type = calcFolderType(filePath);
        if(folderOptions) folder.options = folderOptions;

        // Вычисляем родительский каталог
        let lastFolder = path.split('/');
        lastFolder = lastFolder[lastFolder.length - 1];

        if(lastFolder !== 'p') {
          // Создаем рекурсию для сканирования вложенных каталогов
          scanDir(filePath, folder);
        }

      }

    } // for files

  } // if files.length

} // scanDir

let getFoldersMap = (callback) => {
  return fs.readdir(rootFolder, function(err, domainFolders) {
    let data = {name: `root`};

    scanDir(rootFolder, data);

    callback(data);
  });
}

module.exports = getFoldersMap;
