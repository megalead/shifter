'use strict';

var fs = require('fs');

let renameFolder = (path, newName) => {
  try {
    let splitPath = path.split('/');
    let newPath = splitPath.fill(newName, -1).join('/');

    console.log(`
      Old path: ${path}
      New path: ${newPath}
    `);

    fs.renameSync(path, newPath);
    return newPath;
  } catch (e) {
    throw new Error('Ошибка во время переименования каталога');
  }
}

module.exports = renameFolder;
