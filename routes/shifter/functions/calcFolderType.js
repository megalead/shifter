'use strict';

var fs = require('fs');
var ShifterConstants = require('../ShifterConstants');
var isDirectory = require('./isDirectory');

const rootFolder = ShifterConstants.DOMAINS_DIR;

// /home/silent/public_html/tests/service/daily/powerbank/p/test

const TPL_DIR = ShifterConstants.TPL_DIR;  // Обозначение каталога с шаблонами
const P_DIR = ShifterConstants.P_DIR;      // Обозначение каталога с партнерскими копиями

let getFolderFiles = (path) => {
  try {

    // Проверяем существование каталога
    fs.accessSync(path, fs.F_OK);

    // Получаем статы каталога
    let stats = fs.statSync(path);

    if(stats.isDirectory()) {
      return fs.readdirSync(path);
    } else {
      throw new Error(`Передан не каталог -> ${path}`);
    }

  } catch (e) {
    console.error(`getFolderFiles error ========>>>> ${e.stack}`);
  }
}

let calcFolderType = (path) => {

  if(path) {
    let dirList = [];

    // Разбиваем путь на каталоги
    let arrPath = path.split('/');

    // Вычисляем имя переданного каталога
    let name = arrPath[arrPath.length - 1];

    // Вычисляем родителя каталога
    let parent = arrPath[arrPath.length - 2];

    // Вычисляем тип с текущими данными
    let calcState = false;

    // каталог с ПАРТНЕРСКИМИ КОПИЯМИ
    calcState = (!calcState && name === P_DIR) ? 'partnersFolder' : calcState;

    // каталог ПАРТЕНРА
    calcState = (!calcState && parent === P_DIR) ? 'partnerFolder' : calcState;

    // каталог с ШАБЛОНОМ
    calcState = (!calcState && name === TPL_DIR) ? 'templateFolder' : calcState;

    // Если не удалось вычислить, начинаем сканировать каталог
    if(!calcState) {

      // !!! глобальная переменная
      dirList = getFolderFiles(path);

      // Если каталог содержит каталоги шаблона и партнерских копий
      if(dirList.indexOf(TPL_DIR) !== -1 && dirList.indexOf(P_DIR) !== -1) {
        // То это каталог ПРОЕКТА
        calcState =  'projectFolder';
      }
    }

    // Возможно это каталоги уровня ДОМАЕНА,проверяем каталоги дочерних каталогов
    if(!calcState) {
      let nextFolder = true;

      for(let i = 0; nextFolder && i < dirList.length; i++) {
        let childPath = `${path}/${dirList[i]}`;

        if(isDirectory(childPath)) {
          let childFiles = getFolderFiles(childPath);

          // Дочерний каталог содержит каталоги шаблона и партнерских копий
          if(
            childFiles.indexOf(TPL_DIR) !== -1 &&
            childFiles.indexOf(P_DIR) !== -1
          ) {

            // То это в итоге каталог ДОМЕНА
            calcState =  'domainFolder';
            nextFolder = false;
          }
        }

      }

      // Если каталог расположен в рутовской дирректории
      if(path === `${rootFolder}/${name}`) {
        // То это в итоге каталог ДОМЕНА
        calcState =  'domainFolder';
      }
    }

    return calcState;
  } else {
    throw new Error('Не передан path для вычисления типа каталога');
  }
}

module.exports = calcFolderType;
