'use strict';

var fs = require('fs');

let isDirectory = (path) => {
  try {
    // Проверяем доступ к каталогу заодно и его существование
    fs.accessSync(path, fs.F_OK);
    // Получаем его статы и проверяем евляется ли путь каталогом
    return fs.statSync(path).isDirectory();
  } catch (e) {
    console.error(`isDirectory error ========>>>> ${e.stack}`);
  }
}

module.exports = isDirectory;
