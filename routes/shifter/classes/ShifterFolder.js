'use strict';

var fs = require('fs');
var fse = require('fs-extra');
var readDir = require('../functions/readDir');
var cheerio = require('cheerio');
var fillFolder = require('../functions/fillFolder');
var renameFolder = require('../functions/renameFolder');
var calcFolderType = require('../functions/calcFolderType');
var getFolderFamily = require('../functions/getFolderFamily');
var ShifterConstants = require('../ShifterConstants');
var getFolderOptions = require('../functions/getFolderOptions');
var writeFolderOptions = require('../functions/writeFolderOptions');

class ShifterFolder {
  constructor(props) {
    this._initFolder(props.path || null);
  }

  _initFolder(path) {
    if(path) {
      this._path =  path;

      try {
        // Проверяем доступ к каталогу
        fs.accessSync(this._path, fs.F_OK);

        // Сохраная состояние каталога
        this._folderStats = fs.statSync(this._path);

        if(this._folderStats.isDirectory()) {

          // Инциализируем опции каталога
          this._initOptions();

        } else {throw new Error('Файл не является каталогом');}
      } catch (e) {
        console.log(e.stack);
        throw new Error(`Не удалось получить доступ к каталогу - ${e.message}`);
      }

    } else {
      throw new Error('Не передан путь до каталога');
    }
  }

  _initOptions() {
    this._folderOptions = getFolderOptions(this._path);

    // Если не найден файл с опциями каталога
    if(!this._folderOptions) {
      this._folderOptions = this._createFolderOptionsFile();
    }

    let splitPath = this._path.split('/');
    this._folderOptions.name = splitPath[splitPath.length - 1];
    this._folderOptions.family = getFolderFamily(this._path);

    if(!this._folderOptions.type) {
      this._folderOptions.type = calcFolderType(this._path);
      writeFolderOptions(this._path, this._folderOptions);
    }
  }

  _createFolderOptionsFile() {
    let obj = {type: calcFolderType(this._path),}
    writeFolderOptions(this._path, obj);

    return obj;
  }

  setOptions(options) {
    // Записываем изменения в файл
    writeFolderOptions(this._path, Object.assign(this._folderOptions, options));

    // Инициализируем новые опции
    this._initOptions();

    // Пересабираем index.html файл, с новыми опциями
    return this.prepareTemplate();
  }

  renameFolder(newName) {
    let resultRename = renameFolder(this._path, newName);
    if(resultRename) {
      this._path = resultRename;
      return this._path;
    } else {
      throw new Error('Ошибка при переименовании каталога');
    }
  }

  _findTemplateDir() {
    // Если это каталог партнерской копии, значит шаблон, нах-ся выше уровнем
    if(this._folderOptions.type === 'partnerFolder') {
      // Ищем каталог с шаблоном
      let tplDir = this._folderOptions.family.uncles.find((val, index, arr)=> {
        return val.name === ShifterConstants.TPL_DIR;
      });

      return tplDir;
    }
  }

  fillTemplate() {
    // Ищем каталог с шаблоном
    let tplDir = this._findTemplateDir();
    let fillResult = fillFolder(tplDir.path, this._path);

    // После залития преобразуем index.html файл
    this.prepareTemplate();

    return fillResult;
  }

  // Преабразуем партнерский html файл в php, подставляем ключи и т.п.
  prepareTemplate() {
    let _this = this;
    let tplDir = this._findTemplateDir();
    let tplDirFiles = [];

    try {
      readDir(tplDir.path, (files) => {tplDirFiles = files;});
      if(tplDirFiles.length) {
        // Ищем html файл с шаблоном
        let htmlFile = tplDirFiles.find((value, index, array) => {
          return value === 'index.html';
        });

        if(!htmlFile) throw new Error('Не найден index.html');

        let htmlFilePath = `${tplDir.path}/${htmlFile}`;

        // Проверяем доступ к файлу
        fs.accessSync(htmlFilePath, fs.F_OK);
        let $ = cheerio.load(fs.readFileSync(htmlFilePath), {
          normalizeWhitespace: true,
          decodeEntities: false,
          // xmlMode: true
        });

        let inputNameName = 'Contact';
        let inputNamePhone = 'MobilePhone';


        $('form').each(function(i, elem) {
          // Устанавливаем селекторы
          let $form = $(this);
          let $inputs = $form.find('input');
          let $textarea = $form.find('textarea');

          // Изменяем атрибуты форм и их елементов
          $form.attr('action', 'post_confirm_success.php');
          if($inputs.length === 1) {
            $($inputs[0]).attr('name', inputNameName);
            $($textarea[0]).attr('name', inputNamePhone);

          } else if($textarea.length == 2) {
            $($textarea[0]).attr('name', inputNameName);
            $($textarea[1]).attr('name', inputNamePhone);

          } else {
            $($inputs[0]).attr('name', inputNameName);
            $($inputs[1]).attr('name', inputNamePhone);
          }

          // Добавляем скрытые поля
          $form.append(ShifterConstants.ADD_INPUTS);

          // Вставляем в скрытые поля опции
          let $sourceidInput = $form.find('#sourceId');
          let $productidInput = $form.find('#productId');
          let $partneridInput = $form.find('#partnerId');

          $sourceidInput.val(_this._folderOptions.sourceid || '');
          $productidInput.val(_this._folderOptions.productid || '');
          $partneridInput.val(_this._folderOptions.partnerid || '');

        });

        // Подключаем php скрипт в начало документа
        let out = `${ShifterConstants.HEAD_INCLUDE} ${$.html()}`;

        // Записываем результат в файл index.php
        fs.writeFileSync(`${this._path}/index.php`, out);

        return true;
      }
      throw new Error('Каталог с сайтом партнера пустой');

    } catch (e) {
      console.log(`Неудачная обработка шаблона ====>>>>> ${e.stack}`);
      throw new Error(`${e.message}`);
    }
  }

  createChildFolder(newname) {
    if(!newname) newname = Math.random();
    let newPath = `${this._path}/${newname}`;

    // Если это каталог содержащий партнерские сайты (p)
    if(this._folderOptions.type === 'partnersFolder') {
      try {
        fs.accessSync(newPath, fs.F_OK);
        return false;
      } catch (e) {
        // Каталога не существует. Создаем новый каталог
        fs.mkdirSync(newPath);
        // Инициализируем новый объект каталогов для новенькой папочки
        let Folder = new ShifterFolder({path: newPath});
        // Заливаешь шаблон
        Folder.fillTemplate();

        return true;
      } // /catch
    } // /if
  } // /fun

  fillTemplateToChilds() {
    try {
      let folderChilds = this._folderOptions.family.childs;
      for(let child in folderChilds) {
        let folder = new ShifterFolder({path: folderChilds[child].path});
        folder.fillTemplate();
      }
    } catch (e) {
      console.error(e.stack);
      throw new Error(`Ошибка во время залива: ${e.message}`);
    }

    return {message: 'Шаблон успешно залит во все каталоги'}
  }

  initNewDomainProject(newname) {
    newname = newname || (() => {throw new Error('Не передано имя для каталога')});

    let newProjectFolderPath =  `${this._path}/${newname}`;
    let newTemplateFolderPath = `${this._path}/${newname}/${ShifterConstants.TPL_DIR}`;
    let newPartnersFolderPath = `${this._path}/${newname}/${ShifterConstants.P_DIR}`;

    if(this._folderOptions.type === 'domainFolder') {
      try {
        // Проверяем существование каталога
        fs.accessSync(newProjectFolderPath, fs.F_OK);

        return false;
      } catch (e) {
        // Каталога не существует. Создаем новый каталог
        fs.mkdirSync(newProjectFolderPath);
        fs.mkdirSync(newTemplateFolderPath);
        fs.mkdirSync(newPartnersFolderPath);

        return true;
      }
    }

    return {message: 'Проект инициализирован'}
  }

} // end class

module.exports = ShifterFolder;
