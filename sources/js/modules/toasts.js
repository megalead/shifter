'use strict';

var toasts = document.querySelector('.toasts');
var types = ['error','message','warn','success'];

var addToast = function (type, text) {
  if (type == null) type = 'message';

  var toast = document.createElement('li');

  toast.classList.add(type);
  toasts.appendChild(toast);

  return toast.innerHTML = `${type}. ${text}`;
};

let removeToast = (e) => {
  if (e.animationName === 'hide') {
      return toasts.removeChild(e.target);
  }
}

toasts.addEventListener('animationend', function (e) {removeToast(e)});

module.exports = addToast;
