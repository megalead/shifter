'use strict';

import React from 'react';


class Modal extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return(
      <div className="modal" id="modal-one" aria-hidden="true">
        <div className="modal-dialog card-3">
          <div className="modal-header">
            <h2>{this.props.title}</h2>
            <a href="#close" className="btn-close" aria-hidden="true">×</a>
          </div>
          <div className="modal-body">
            {this.props.contentComponent}
          </div>
          {/*<div className="modal-footer">
            <a
              href="#close"
              onClick={this.handleClickComplete.bind(this)}
              className="btn"
            >Готово</a>
          </div>*/}
        </div>
      </div>
    );
  }

  // Обрабтка события на нажатие кнопки - Готово
  handleClickComplete(event) {
    this.props.onClickComplete(event);
  }
}

Modal.defaultProps = {
  title: 'Модальное окно',

  onClickComplete: () => console.info('Set prop onClickComplete for <Modal />'),
}

Modal.propTypes = {
  title: React.PropTypes.string,
  onClickComplete: React.PropTypes.func,
  contentComponent: React.PropTypes.object,
}

export default Modal;
