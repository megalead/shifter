'use strict';

import $ from 'jquery';
import React from 'react';
import addToast from 'modules/toasts';

class ShifterDomainFolderContent extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return(
      <div>
        <span className="text-subhead">Инициализировать каталоги проекта</span>
        <div style={{display: 'flex'}}>
          <input
            ref={(c) => this._newNameInput = c}
            type="text"
            style={{flexGrow: 3, marginRight: 16}}
            placeholder="Имя каталога нового проекта"
          />
          <button
            style={{flexGrow: 1}}
            className="btn"
            onClick={this.handleClickInitNewDomainProject.bind(this)}
          >
            Создать
          </button>
        </div>
      </div>
    );
  }

  handleClickInitNewDomainProject(event) {
    // Проверяем валидное название для каталога
    var rx = /[<>:"\/\\|?*\x00-\x1F]|^(?:aux|con|clock\$|nul|prn|com[1-9]|lpt[1-9])$/i;
    if(rx.test(this._newNameInput.value) || !this._newNameInput.value) {
      this._newNameInput.value = "";
      return addToast('error', 'Ну нельзя так называть папку!');
    }

    // Отправляем запрос на сервер создать новый проект
    $.get(`/shifter/initNewDomainProject`, {
      path: this.props.path,
      newname: this._newNameInput.value
    })
      .done((res) => {
        // Обвновляем дерево
        this.props.onLoadedNewTree(res.foldersMap);
        return addToast('success', `${res.message}`);
      })
      .fail((res) => {
        let data = res.responseJSON;
        if('foldersMap' in data) this.props.onLoadedNewTree(data.foldersMap);

        // console.log('Fail', res);
        return addToast('error', `${data.message || 'Ошибка во время запроса'}`);
      });
  }
}

export default ShifterDomainFolderContent;
