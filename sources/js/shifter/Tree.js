'use strict';

import $ from 'jquery';
import React from 'react';
import TreeUl from './TreeUl';
import backendHost from '../configs/host';

class Tree extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      foldersMap: this.props.foldersMap,
    }
  }

  shouldComponentUpdate(nextProps, nextState) {
    return true;
    // return nextProps.path !== this.props.path;
  }

  componentWillReceiveProps(nextProps) {
    this.setState(nextProps);
  }

  render() {
    return(
      <div className="tree">
        <ul>
          <li>
            <a href="#">root</a>
            <TreeUl onSelectFolder={this.props.onSelectFolder} data={this.state.foldersMap} />
          </li>
        </ul>
      </div>
    );
  }
}

export default Tree;
