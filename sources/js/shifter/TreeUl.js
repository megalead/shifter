'use strict';

import React from 'react';
import TreeLi from './TreeLi';

class TreeUl extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {

    let name = this.props.data.name ? this.props.data.name : 'no name';
    let repeats = <li><a href="#">{name}</a></li>;

    if(this.props.data.folders.length) {
      repeats = this.props.data.folders.map((element) => {
        return(
          <TreeLi key={element.name} id={element.name}>
            <a
              href="#modal-one"
              onClick={this.handleClickFolder.bind(this, element)}
            >
              {element.name}
            </a>
            {(() => {
              if(element.folders.length) {
                return(
                  <TreeUl
                    onSelectFolder={this.props.onSelectFolder}
                    data={element}
                  />
                );
              }
            })()}
          </TreeLi>
        );
      });
    }

    return(
      <ul>
        {repeats}
      </ul>
    );
  }

  handleClickFolder(selectedFolder) {
    this.props.onSelectFolder(selectedFolder);
  }
}

export default TreeUl;
