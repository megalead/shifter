'use strict';

import $ from 'jquery';
import React from 'react';
import addToast from 'modules/toasts';

class ShifterPartnersFolderContent extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      newFolderName : '',
    }
  }

  render() {
    return(
      <div>
        <span className="text-subhead">Создать новый партнерский каталог</span>
        <div style={{display: 'flex'}}>
          <input
            type="text"
            style={{width: '60%'}}
            value={this.state.newFolderName}
            onChange={this.handleChangeNewFolderName.bind(this)}
            placeholder="Название каталога"
          />
          <button
            style={{marginLeft: 6, padding: '2px 15px'}}
            onClick={this.handleClickCreateNewFolder.bind(this)}
            className="btn"
          >
            Новый партнерский каталог
          </button>
        </div>

        <hr/>

        <div >
          <button
            style={{width: '100%'}}
            onClick={this.handleFillTemplateToChilds.bind(this)}
            className="btn"
          >
            Залить дизайн во все дочерние каталоги
          </button>
        </div>
      </div>
    );
  }

  handleFillTemplateToChilds(event) {
    $.get(`/shifter/fillTemplateToChilds`, {path: this.props.path,})
      .done((res) => {
        return addToast('success', `${res.message}`);
      })
      .fail((res) => {
        let data = res.responseJSON;
        return addToast('error', `${data.message || 'Ошибка во время запроса'}`);
      });
  }

  handleChangeNewFolderName(event) {
    this.setState({newFolderName: event.target.value, __proto__ : this.state});
  }

  handleClickCreateNewFolder(event) {
    // Проверяем валидное название для каталога
    var rx = /[<>:"\/\\|?*\x00-\x1F]|^(?:aux|con|clock\$|nul|prn|com[1-9]|lpt[1-9])$/i;
    if(rx.test(this.state.newFolderName) || !this.state.newFolderName) {
      return addToast('error', 'Ну нельзя так назвать папку!');
    }

    $.get(`/shifter/createNewPartnerFolder`, {
      path: this.props.path,
      newname: this.state.newFolderName,
    })
      .done((res) => {
        // Обвновляем дерево
        this.props.onLoadedNewTree(res.foldersMap);
        return addToast('success', `${res.message}`);
      })
      .fail((res) => {
        let data = res.responseJSON;
        if('foldersMap' in data) this.props.onLoadedNewTree(data.foldersMap);

        // console.log('Fail', res);
        return addToast('error', `${data.message || 'Ошибка во время запроса'}`);
      });
  }

}

export default ShifterPartnersFolderContent;
