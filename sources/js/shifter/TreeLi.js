'use strict';

import React from 'react';

class TreeLi extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return <li>{this.props.children}</li>
  }

}

export default TreeLi;
