'use strict';

import $ from 'jquery';
import React from 'react';
import addToast from 'modules/toasts';

class ShifterPartnerFolderContent extends React.Component {
  constructor(props) {
    super(props);

    this.state = this.props;
  }

  shouldComponentUpdate(nextProps, nextState) {
    return true;
    // return nextProps.path !== this.props.path;
  }

  componentWillReceiveProps(nextProps) {
    this.setState(nextProps);
  }

  render() {
    let inputs = [
      {name: 'sourceid', label: 'Ключ источника лида'},
      {name: 'productid', label: 'Ключ товара'},
      // {name: 'partnerid', label: 'Ключ партнера'},
    ];

    let buttonStyle = {width: '80%', marginTop: 6,};

    return(
      <div style={{display: 'flex'}}>
        <form onSubmit={this.handleSubmitFolderOptions.bind(this)}>
          <span className="text-subhead">Параметры</span>

          {/* Выводим поля параметров */}
          {
            inputs.map((input) => {
              return(
                <div key={input.name}>
                  <label htmlFor={`input-${input.name}`}>{input.label}</label>
                  <input
                    id={`input-${input.name}`}
                    type="text"
                    name={input.name}
                    onChange={this.handleChangeOptionsInputs.bind(this)}
                    placeholder={input.name}
                    value={this.state.options[input.name] || ''}
                  />
                </div>
              );
            })
          }
          <br />
          <input type="submit" value="Сохранить параметры" className="btn"/>
        </form>
        <div style={{paddingLeft: 16,}}>
          <buttont
            style={buttonStyle}
            className="btn"
            onClick={this.handleFillTemplate.bind(this)}
          >
            Залить файлы шаблона
          </buttont>

          <hr/>

          {/*<buttont className="btn" style={buttonStyle}>Клонировать</buttont>*/}

          <span className="text-subhead">Переименовать</span>
          <div style={{display: 'flex'}}>
            <input
              type="text"
              onChange={this.handleChangeNameFolderInput.bind(this)}
              value={this.state.name}
              style={{width: '60%'}}
            />
            <button
              style={{marginLeft: 6, padding: '2px 15px'}}
              onClick={this.handleRenameSubmit.bind(this)}
              className="btn"
            >
              Ок
            </button>
          </div>

          <hr/>

          <span className="text-subhead">Залить файл post_confirm</span>
          <div>
            <form onSubmit={this.handleSubmitUploadPostConfirmFile.bind(this)}>
              <input type="hidden" name="path" defaultValue={this.state.path}/>
              <input type="file" name="file" required />
              <input
                type="submit"
                name="file"
                style={{marginTop:8, width: '100%'}}
                className="btn"
                defaultValue="Загрузить"
              />
            </form>
          </div>

        </div>
      </div>
    );
  }

  handleSubmitUploadPostConfirmFile(event) {
    event.preventDefault();
    let fd = new FormData(event.target);
    // fd.append("path", this.state.path);
    $.ajax({
      url: "shifter/uploadPostConfirmFile",
      type: "POST",
      data: fd,
      processData: false,  // tell jQuery not to process the data
      contentType: false   // tell jQuery not to set contentType
    })
      .done((result) => {
        addToast('success', result.message);
      })
      .fail((result) => {
        let data = res.responseJSON;
        return addToast('error', `${data.message || 'Ошибка во время запроса'}`);
      });
  }

  /**
   * Триггер на отправку запроса залить шаблон в выбранный каталог
   * @param  {Default event} event Обычное Javascript событие на клик по кнопке
   * @return {Promise request}       результат обработки запроса
   */
  handleFillTemplate(event) {
    $.get('shifter/fillTemplate', {path: this.state.path, name: this.state.name})
      .done((result) => {
        addToast('success', result.message);
      })
      .fail((result) => {
        addToast('error', result.message);
      });
  }

  /**
   * Триггер на отправку запроса переименовать выбранный каталог
   * @param  {Default event} event Обычное Javascript событие на клик по кнопке
   * @return {Promise request}       результат обработки запроса
   */
  handleRenameSubmit(event) {
    // Проверяем валидное название для каталога
    var rx = /[<>:"\/\\|?*\x00-\x1F]|^(?:aux|con|clock\$|nul|prn|com[1-9]|lpt[1-9])$/i;
    if(rx.test(this.state.name) || !this.state.name) {
      this.setState({name: this.props.name, __proto__ : this.state});
      return addToast('error', 'Ну нельзя так назвать папку!');
    }

    $.get('shifter/renameFolder', {path: this.state.path, name: this.state.name})
      .done((result) => {
        this.setState({path: result.path, __proto__ : this.state});
        this.props.onRenameSelectedFolder({path: result.path, name: this.state.name});
        addToast('success', result.message);
      })
      .fail((result) => {
        this.setState({name: this.props.name, __proto__ : this.state});
        addToast('error', result.message);
      });
  }

  /**
   * Тригер для изменения параметров каталога в состоянии компоненты
   * @param  {Default event} event Обычное Javascript событие на изменение input
   * @return {void}       Обновляет состояние state.options
   */
  handleChangeOptionsInputs(event) {
    let state = this.state;
    state.options[event.target.name] = event.target.value;
    this.setState(state);
  }

  /**
   * Тригер для изменения состояния имени каталога через input
   * @param  {Default event} event Обычное Javascript событие на изменение input
   * @return {void}       Обновляет состояние state.name
   */
  handleChangeNameFolderInput(event) {
    this.setState({name: event.target.value, __proto__ : this.state});
  }

  /**
   * Триггер на отправку запроса обновления параметров каталога
   * @param  {Default event} event Обычное Javascript событие submit формы
   * @return {Promise request}       результат обработки запроса
   */
  handleSubmitFolderOptions(event) {
    event.preventDefault();

    let data = {
      path: this.props.path,
      options: this.state.options,
    }

    $.post('/shifter/saveFolderOptions', data)
      .done((result) => {
        addToast('success', result.message);
      })
      .fail((result) => {
        addToast('error', result.message);
      });
  }
}

ShifterPartnerFolderContent.defaultProps = {
  path: '',
  options: {
    sourceid: '',
    productid: '',
    partnerid: '',
  },
};
ShifterPartnerFolderContent.propTypes = {data: React.PropTypes.object,};

export default ShifterPartnerFolderContent;
