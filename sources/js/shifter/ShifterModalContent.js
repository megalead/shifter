'use strict';

import React from 'react';
import ReactDOM from 'react-dom';
import ShifterDomainFolderContent from './ShifterDomainFolderContent';
import ShifterPartnerFolderContent from './ShifterPartnerFolderContent';
import ShifterPartnersFolderContent from './ShifterPartnersFolderContent';

class ShifterModalContent extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    let data = this.props.data;
    let type = '';
    let noComponentMes = 'Для данного типа каталога, функционала,пока нет';

    // Разные типы каталогов могут содержать различные формы, методы и т.п.
    this._folderComponent = <div>{noComponentMes}</div>;

    let folderProps = {
      options : data.options,
      path: data.path,
      name: data.name,
      onRenameSelectedFolder: this.props.onRenameSelectedFolder,
      onLoadedNewTree: this.props.onLoadedNewTree,
    }

    switch (data.type) {
      case 'domainFolder':
        type = 'Каталог домена';
        this._folderComponent = <ShifterDomainFolderContent {...folderProps}/>;
        break;

      case 'projectFolder':
        type = 'Каталог проекта';
        break;

      case 'templateFolder':
        type = 'Каталог оригинала';
        break;

      case 'partnersFolder':
        type = 'Каталог с партнерскими копиями';
        this._folderComponent = <ShifterPartnersFolderContent {...folderProps}/>;
        break;

      case 'partnerFolder':
        type = 'Партнерская копия';
        this._folderComponent = <ShifterPartnerFolderContent {...folderProps}/>;
        break;

      default:
        type = 'Неизвестный тип';
    }

    return(
      <div>
        <p>
          <strong>Имя:</strong> {data.name} <br />
          <strong>Каталог:</strong> {data.path} <br />
          <strong>Тип каталога:</strong> {type}
        </p>

        <div>
          {this._folderComponent}
        </div>

      </div>
    );
  }
}

export default ShifterModalContent;
