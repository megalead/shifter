'use strict';

import $ from 'jquery';
import Tree from './shifter/Tree';
import React from 'react';
import ReactDOM from 'react-dom';
import Modal from './modules/modal/Modal';
import ShifterModalContent from './shifter/ShifterModalContent';

class Shifter extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      selectedFolder: '',
      foldersMap: {
        name: ``,
        folders: [],
      },
    }
  }

  render() {
    return(
      <div>
        {/* Отрисовываем дерево каталогов */}
        <Tree
          onSelectFolder={this.handleSelectFolder.bind(this)}
          selectedFolder={this.state.selectedFolder}
          foldersMap={this.state.foldersMap}
        />

        {/* Инициализируем мадальное окно и передаем необходимые контент компоненту */}
        <Modal
          onClickComplete = {this.handleClickComplete.bind(this)}
          contentComponent = {
            <ShifterModalContent
              data={this.state.selectedFolder}
              onRenameSelectedFolder = {this.handleRenameSelectedFolder.bind(this)}
              onLoadedNewTree={this.handleWasLoadedNewTree.bind(this)}
            />
          }

        />
      </div>
    );
  }

  componentDidMount() {
    // Загружаем дерево каталогов
    this.serverRequest = $.get(`/shifter/getTree`, function (result) {
      console.log('Server request', result);
      this.setState({foldersMap: result.foldersMap, __proto__: this.state});
      console.log('Current state', this.state);
    }.bind(this));
  }

  // Была подгружено новое дерево
  handleWasLoadedNewTree(newTree) {
    this.setState({foldersMap: newTree, __proto__: this.state});
  }

  handleRenameSelectedFolder(prop) {
    let selectedFolder = this.state.selectedFolder;
    selectedFolder.name = prop.name || 'not give new name';
    selectedFolder.path = prop.path || 'not give new path';

    this.setState(Object.assign(this.state,{selectedFolder: selectedFolder,}));
  }

  // Событие отрабатывает после клика на ссылку каталого в дереве
  handleSelectFolder(selectedFolder) {
    this.setState(Object.assign(this.state, {selectedFolder: selectedFolder}));
  }

  handleClickComplete(event) {

  }
}

ReactDOM.render(
  <Shifter />,
  document.getElementById('app')
);
