'use strict';

/**
 *
 * # Запуск сборки в продакшн
 * $ NODE_ENV=production webpack
 *
 * # Создать файл stats.json для анализа на http://webpack.github.io/analyse
 * $ webpack --json --profile > stats.json
 *
 * # Детальное отображение используемых модулей
 * $ webpack --display-modules -v
 *
 * # Детальное отображене с времением компиляции
 * $ webpack --profile --display-modules --display-reasons
 */

var webpack = require('webpack');

// Тип сборки
const NODE_ENV = process.env.NODE_ENV || 'development';
const BACKEND_HOST = 'http://localhost:3000';

// Костанты для указания их в плагине
const lang = JSON.stringify("ru"),
      node_env = JSON.stringify(NODE_ENV);

module.exports = {

  // Каталог контекста
  context: __dirname + '/sources/js',

  // Входные файлы
  entry: {
    shifter: [
      'webpack-dev-server/client?http://localhost:8080',
      'webpack/hot/dev-server',
      './shifter'],
    common: './common',
  },

  // Выходные файлы
  output: {
    path: __dirname + '/public',
    library: "[name]",
    filename: '[name].js',
  },

  // Карта скриптов
  //devtool: NODE_ENV == 'development' ? 'cheap-inline-module-source-map' : null,
  devtool: NODE_ENV == 'development' ? 'eval' : null,

  // Отслеживание изменений
  watch: NODE_ENV == 'development',
  watchOptions: {aggregateTimeout: 100,},

  // Подключаем дополнительные плагины
  plugins: [
    new webpack.DefinePlugin({ LANG: lang, NODE_ENV: node_env,}),
    new webpack.EnvironmentPlugin(['USER']),

    // Не компилить файлы при ошибках
    new webpack.NoErrorsPlugin(),

    // Создает скрипт, содержащий модули, которые испол-ся во всех точках входа
    // примеры использования => https://goo.gl/WE1eCH
    new webpack.optimize.CommonsChunkPlugin({
      name: 'common',
    }),

    new webpack.HotModuleReplacementPlugin(),
  ],

  // Параметры поиска плагинов
  resolve: {
    extensions: ['' , '.js'],
    modulesDirectories: ['node_modules', './sources/js/'],
  },
  resolveLoader: {
    extensions: ['', '.js'],
    moduleTemplates: ['*-loader', '*'],
    modulesDirectories: ['node_modules'],
  },

  // Объявление дополнительных модулей
  module: {
    loaders: [{
      test: /\.js$/,
      loader: 'babel',
      loaders: ['react-hot', 'babel'],
      include: __dirname + '/sources/js', // Пропускаем через Babel только наши модули
      query: {
        presets: ['es2015', 'react'],
      }
    }],
  },

  devServer: {
    contentBase: 'public',
    publicPath: '/',
    port: 8080,
    headers: {
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept'
    },
    hot: true,
    proxy: {
      "*" : "http://localhost:3000",
    }
  }
}

// Минимизируем скрипты если запускается прадакшн кампиляция
if (NODE_ENV == 'production') {
  module.exports.plugins.push(
    new webpack.optimize.UglifyJsPlugin({
      compress: {
        unsafe: true,
        warnings: false,
        drop_console: true,
      }
    })
  );
}
